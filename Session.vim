let SessionLoad = 1
if &cp | set nocp | endif
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
let NERDSpaceDelims = "0"
let NERDCommentWholeLinesInVMode = "0"
let NERDRemoveExtraSpaces = "1"
let NERDUsePlaceHolders = "1"
let NERDCreateDefaultMappings = "1"
let NERDMenuMode = "3"
let NERDDefaultNesting = "1"
let NERDCompactSexyComs = "0"
let NERDBlockComIgnoreEmpty = "0"
let NERDDelimiterRequests = "1"
let NERDRemoveAltComs = "1"
let NERDRPlace = "<]"
let NERDLPlace = "[>"
let NERDDefaultAlign = "none"
let NERDCommentEmptyLines = "0"
let NERDTrimTrailingWhitespace = "0"
let RunFile = "clear; g++ -g avl_tree.cpp && ./a.out"
let NERDAllowAnyVisualDelims = "1"
silent only
cd ~/Projects/AVL
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
badd +1 avl_tree.h
badd +1 avl_tree.cpp
argglobal
silent! argdel *
argadd avl_tree.h
set stal=2
edit avl_tree.cpp
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
setlocal fdm=manual
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=0
setlocal fml=1
setlocal fdn=20
setlocal fen
silent! normal! zE
let s:l = 193 - ((17 * winheight(0) + 11) / 22)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
193
normal! 0
tabedit avl_tree.h
set splitbelow splitright
set nosplitbelow
set nosplitright
wincmd t
set winheight=1 winwidth=1
argglobal
setlocal fdm=indent
setlocal fde=0
setlocal fmr={{{,}}}
setlocal fdi=#
setlocal fdl=5
setlocal fml=1
setlocal fdn=20
setlocal fen
21
normal! zo
27
normal! zo
28
normal! zo
27
normal! zc
47
normal! zo
50
normal! zc
56
normal! zc
67
normal! zo
67
normal! zc
74
normal! zo
74
normal! zc
90
normal! zo
91
normal! zc
90
normal! zc
96
normal! zo
96
normal! zc
106
normal! zc
125
normal! zo
127
normal! zc
138
normal! zo
147
normal! zo
154
normal! zc
138
normal! zc
167
normal! zo
167
normal! zc
175
normal! zo
176
normal! zo
184
normal! zo
175
normal! zc
196
normal! zo
196
normal! zc
216
normal! zo
216
normal! zc
237
normal! zo
237
normal! zc
318
normal! zo
322
normal! zc
318
normal! zc
330
normal! zo
330
normal! zc
343
normal! zo
343
normal! zc
356
normal! zo
356
normal! zc
383
normal! zc
390
normal! zo
410
normal! zo
410
normal! zc
390
normal! zc
433
normal! zo
448
normal! zo
460
normal! zo
481
normal! zc
487
normal! zo
488
normal! zc
487
normal! zc
503
normal! zc
let s:l = 433 - ((104 * winheight(0) + 22) / 45)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
433
normal! 09|
tabnext 2
set stal=1
if exists('s:wipebuf')
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToO
let s:sx = expand("<sfile>:p:r")."x.vim"
if file_readable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
