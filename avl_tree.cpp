#include "avl_tree.h"

#include <random>
#include <ctime>
 
int main(int argc, char **argv)
{

    std::srand(std::time(0)); // use current time as seed for random generator

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()

    int tree_size = 10;
    int maxval = 99;

    std::uniform_int_distribution<> dis(0, maxval);

    AVL::Tree<int> tree;

    /*
     *for (int i=0; i<tree_size; i++) {
     *    tree.insert(dis(gen));
     *}
     */


    tree.insert(50);
    tree.insert(25);
    tree.insert(12);
    tree.insert(11);
    tree.insert(37);
    tree.insert(75);
    tree.insert(62);
    tree.insert(87);

    /*
     *tree.insert(8);
     *tree.insert(3);
     *tree.insert(2);
     *tree.insert(1);
     *tree.insert(5);
     *tree.insert(4);
     *tree.insert(7);
     *tree.insert(6);
     *tree.insert(10);
     *tree.insert(9);
     *tree.insert(12);
     *tree.insert(11);
     *tree.insert(13);
     */

    tree.print_preorder();
    //int i = 12;
    //int i = 25;
    //int i = 50;
    int i = 75;
    cout << "=============" << endl;
    cout << "Deleting " << i << endl;
    cout << "=============" << endl;
    tree.remove(i);
    tree.print_preorder();

    return 0;
}










/*
 * Below is the old AVL implementation
 */

typedef struct node
{
    int val;
    struct node* left;
    struct node* right;
    int ht;
} node;


int ht(node *n)
{
    return (n) ? n->ht : -1;
}

void updateHeight(node *n)
{
    if (n == nullptr)
        return;
    n->ht = (ht(n->left) > ht(n->right)) ? ht(n->left)+1 : ht(n->right)+1;
}

int balanceFactor(node *n)
{
    return (n) ? ht(n->left) - ht(n->right) : 0;
}

node *rol(node *n)
{
    node *x, *y, *a, *b, *c;
    x = n;
    y = n->right;
    if (y == nullptr)
        return n;
    a = x->left;
    b = (y) ? y->left : nullptr;
    c = (y) ? y->right : nullptr;
    node *ret = y;
    ret->left = x;
    x->left = a;
    x->right = b;
    ret->right = c;
    /* Adjust heights of Y and X. */
    updateHeight(ret->left);
    updateHeight(ret);
    return ret;
}

node *ror(node *n)
{
    node *x, *y, *b, *c, *d;
    x = n;
    y = n->left;
    if (y == nullptr)
        return n;
    b = (y) ? y->left : nullptr;
    c = (y) ? y->right : nullptr;
    d = x->right;
    node *ret = y;
    ret->right = x;
    ret->left = b;
    x->left = c;
    x->right = d;
    /* Adjust heights of Y and X. */
    updateHeight(ret->right);
    updateHeight(ret);
    return ret;
}

node *balance(node *root)
{
    if (balanceFactor(root) == -2) {
        if (balanceFactor(root->right) == 1) {
            /* Right-left case. */
            root->right = ror(root->right);
        }
        /* Right-right case. */
        root = rol(root);
    }
    else if (balanceFactor(root) == 2) {
        if (balanceFactor(root->left) == -1) {
            /* Left-right case. */  
            root->left = rol(root->left);
        }
        /* Left-left case. */
        root = ror(root);
    }
    return root;
}

node *insert(node *root, int val)
{
    /* If reached a nullptr node, we should insert here. */
    if (root == nullptr) {
        node *new_node = new node;
        new_node->val = val;
        new_node->left = nullptr;
        new_node->right = nullptr;
        new_node->ht = 0;
        return new_node;
    }
        
    /* If the inserted value is smaller, recurse left. */
    if (val < root->val) {
        root->left = insert(root->left, val);
    }
    /* Otherwise, recurse right. */
    else {
        root->right = insert(root->right, val);
    }
    
    /* Update the current height after inserting a node. */
    updateHeight(root);
    
    /* Now, after inserting the node and updating its height,
     * we can compare its subtrees' heights and perform
     * rotations as necessary.
     */
    root = balance(root);

    /* Eventually, return the root. */
    return root;
}

