#ifndef _AVL_TREE_
#define _AVL_TREE_

#include <algorithm> // std::max
#include <deque>
#include <iostream>
#include <string>
#include <sstream>
#include <regex>
using namespace std;

namespace AVL {

/*
 * This class holds information that is local to a node. E.g. the node's
 * height or depth.
 */
class LocalDescriptorTable {
private:
public:
    // Distance from root.
    //int depth;
    // Distance from farthest leaf.
    int height;
    LocalDescriptorTable(LocalDescriptorTable *other)
    {
        if (other == nullptr) {
            //depth = -1;
            height = -1;
        }
        else {
            *this = *other;
        }
    }
    LocalDescriptorTable() : LocalDescriptorTable(nullptr)
    {
    }
};

template <class T>
class Node {
    T data;
    Node<T> *parent;
    Node<T> *left;
    Node<T> *right;
public:
    LocalDescriptorTable local;
    Node()
    {
        parent = nullptr;
        left = nullptr;
        right = nullptr;
    }
    Node(T data) : Node()
    {
        this->data = data;
    }
    ~Node()
    {

    }
    void setData(const T &data) { this->data = data; }
    void setParent(Node<T> *parent) { this->parent = parent; }
    // Sets the left child, and also sets left's parent to this.
    void setLeft(Node<T> *left)
    {
        this->left = left;
        if (left) {
            left->setParent(this);
        }
    }
    // Sets the right child, and also sets right's parent to this.
    void setRight(Node<T> *right) {
        this->right = right;
        if (right) {
            right->setParent(this);
        }
    }
    T &getData() { return this->data; }
    Node<T> *getParent() { return this->parent; }
    Node<T> *getLeft() { return this->left; }
    Node<T> *getRight() { return this->right; }
    int getChildrenNumber()
    {
        return (getLeft() != nullptr) + (getRight() != nullptr);
    }

    bool operator>(const Node<T>& other) const
    {
        if (this->data > other.data)
            return true;
        return false;
    }
    bool operator==(const Node<T>& other) const
    {
        if (this->data == other.data)
            return true;
        return false;
    }
    bool operator!=(const Node<T>& other) const { return !(*this == other); }
    bool operator>=(const Node<T>& other) const { return (*this == other || *this > other); }
    bool operator<(const Node<T>& other) const { return !(*this >= other); }
    bool operator<=(const Node<T>& other) const { return (*this < other || *this == other); }
    void updateLocalData()
    {
        LocalDescriptorTable parentLocalDesc( (getParent()) ? getParent()->local : nullptr);
        LocalDescriptorTable leftLocalDesc( (getLeft()) ? getLeft()->local : nullptr);
        LocalDescriptorTable rightLocalDesc( (getRight()) ? getRight()->local : nullptr);

        // Update height.
        local.height = 1 + max(leftLocalDesc.height, rightLocalDesc.height);

        // Update depth.
        //local.depth = 1 + parentLocalDesc.depth;
    }
}; // Node.

template <class T>
class Tree {
    Node<T> *root;

    size_t size;

public:
    Tree()
    {
        root = nullptr;
        size = 0;
    }

    size_t getSize() { return size; }

    void setRoot(Node<T> *newroot) { root = newroot; }

    // Get a deque with pointers to all the tree nodes, ordered by depth.
    deque<Node<T> *> getBFS()
    {
        deque<Node<T> *> open;
        deque<Node<T> *> closed;

        if (getSize() == 0) {
            return closed; // Empty.
        }

        open.push_back(root);
        while (open.size() != 0) {
            Node<T> *curr = open.front();
            open.pop_front();
            closed.push_back(curr);

            Node<T> *l = curr->getLeft();
            Node<T> *r = curr->getRight();
            if (l) {
                open.push_back(l);
            }
            if (r) {
                open.push_back(r);
            }

        }

        return closed;
    }

    int balanceFactor(Node<T> *n)
    {
        if (n == nullptr) {
            return 0;
        }
        auto ht = [](Node<T> *x) { return (x) ? x->local.height : -1; };
        return (n) ? ht(n->getLeft()) - ht(n->getRight()) : 0;
    }
    Node<T> *balance(Node<T> *n)
    {
        if (balanceFactor(n) == -2) {
            if (balanceFactor(n->getRight()) == 1) {
                // Right-left case.
                n->setRight(rotateRight(n->getRight()));
            }
            // Right-right case.
            n = rotateLeft(n);
        }
        else if (balanceFactor(n) == 2) {
            if (balanceFactor(n->getLeft()) == -1) {
                // Left-right case.
                n->setLeft(rotateLeft(n->getLeft()));
            }
            // Left-left case.
            n = rotateRight(n);
        }
        return n;
    }

    Node<T> *rotateLeft(Node<T> *n)
    {
        Node<T> *x, *y, *a, *b, *c;
        x = n;
        y = n->getRight();
        if (y == nullptr)
            return n;
        a = x->getLeft();
        b = (y) ? y->getLeft() : nullptr;
        c = (y) ? y->getRight() : nullptr;
        Node<T> *ret = y;
        ret->setLeft(x);
        x->setLeft(a);
        x->setRight(b);
        ret->setRight(c);
        // Adjust heights of Y and X.
        if (ret->getLeft()) { ret->getLeft()->updateLocalData(); }
        ret->updateLocalData();
        return ret;
    }
    Node<T> *rotateRight(Node<T> *n)
    {
        Node<T> *x, *y, *b, *c, *d;
        x = n;
        y = n->getLeft();
        if (y == nullptr)
            return n;
        b = (y) ? y->getLeft() : nullptr;
        c = (y) ? y->getRight() : nullptr;
        d = x->getRight();
        Node<T> *ret = y;
        ret->setRight(x);
        ret->setLeft(b);
        x->setLeft(c);
        x->setRight(d);
        // Adjust heights of Y and X.
        if (ret->getRight()) { ret->getRight()->updateLocalData(); }
        ret->updateLocalData();
        return ret;
    }

    void swapNodes(Node<T> *n1, Node<T> *n2)
    {
        // Nothing to swap...
        if (n1 == nullptr || n2 == nullptr || n1 == n2) {
            return;
        }
        if (n2 == root) {
            Node<T> *temp = n1;
            n1 = n2;
            n2 = temp;
        }
        // We can now assume that:
        // n1->data.depth <= n2->data.depth.

        Node<T> *p1 = n1->getParent();
        Node<T> *p2 = n2->getParent();
        Node<T> *n1lc = n1->getLeft();
        Node<T> *n1rc = n1->getRight();
        Node<T> *n2lc = n2->getLeft();
        Node<T> *n2rc = n2->getRight();

        // Link the parents.
        if (p1 == nullptr) {
            setRoot(n2);
            n2->setParent(nullptr);
        }
        else if (p1 && p1->getLeft() == n1) {
            p1->setLeft(n2);
            //n2->setParent(p1);
        }
        else {
            p1->setRight(n2);
            //n2->setParent(p1);
        }

        if (p2 && p2->getLeft() == n2) {
            p2->setLeft(n1);
            //n1->setParent(p2);
        }
        else {
            p2->setRight(n1);
            //n1->setParent(p2);
        }

        // Link the children.
        //if (n2lc) {
        //    n2lc->setParent(n1);
        //}
        //if (n2rc) {
        //    n2rc->setParent(n1);
        //}
        //if (n1lc) {
        //    n1lc->setParent(n2);
        //}
        //if (n1rc) {
        //    n1rc->setParent(n2);
        //}
        n1->setLeft(n2lc);
        n1->setRight(n2rc);
        n2->setLeft(n1lc);
        n2->setRight(n1rc);

        // Fix case where one node is a child of the other.
        if (n2 == n1lc) {
            n2->setLeft(n1);
            //n1->setParent(n2);
        }
        else if (n2 == n1rc) {
            n2->setRight(n1);
            //n1->setParent(n2);
        }

        // Swap the nodes themselves.
        //Node<T> *temp = n1;
        //n1 = n2;
        //n2 = n1;

    }

    /* Get minimum node in subtree. */
    Node<T> *getMin() { return getMin(root); }
    Node<T> *getMin(Node<T> *curr)
    {
        if (root == nullptr || curr == nullptr) {
            return nullptr;
        }
        while (curr->getLeft() != nullptr) {
            curr = curr->getLeft();
        }
        return curr;
    }
    /* Get maximum node in subtree. */
    Node<T> *getMax() { return getMax(root); }
    Node<T> *getMax(Node<T> *curr)
    {
        if (root == nullptr || curr == nullptr) {
            return nullptr;
        }
        while (curr->getRight() != nullptr) {
            curr = curr->getRight();
        }
        return curr;
    }

    /* Print the tree in pre-order. */
    void print_preorder() { print_preorder(root); }
    void print_preorder(Node<T> *curr)
    {
        if (curr == nullptr) {
            cout << "null" << endl;
            return;
        }
        cout << curr->getData();
        //cout << "    depth:" << curr->local.depth;
        cout << "    height:" << curr->local.height;
        cout << endl;
        print_preorder(curr->getLeft());
        print_preorder(curr->getRight());
    }

    Node<T> *insert_rec(Node<T> *curr, Node<T> *prev, Node<T> *node)
    {
        if (curr == nullptr) {
            // Node should be inserted here...
            size = size + 1;
            Node<T> *new_node = new Node<T>(node->getData());
            new_node->setParent(prev);
            new_node->updateLocalData();
            return new_node;
        }
        // If the node to insert is smaller than current node, recurse left.
        if (*node < *curr) {
            curr->setLeft( insert_rec(curr->getLeft(), curr, node) );
        }
        // Otherwise, recurse right.
        else {
            curr->setRight( insert_rec(curr->getRight(), curr, node) );
        }
        // Update heights, etc.
        curr->updateLocalData();
        // Now, after inserting the node and updating its height,
        // we can compare its subtrees' heights and perform
        // rotations as necessary.
        curr = balance(curr);

        return curr;
    }
    void insert(const T &data)
    {
        Node<T> nodeToInsert(data);
        this->root = insert_rec(this->root, nullptr, &nodeToInsert);
    }

    // Delete a node with only one child, or no children at all.
    void deleteNodeWithOneChildOrNone(Node<T> *n)
    {
        if (n == nullptr) {
            return;
        }
        // Sanity check. Node actually has two children for some reason.
        if (n->getChildrenNumber() == 2) {
            return;
        }
        Node<T> *child;
        if (n->getLeft() != nullptr) {
            child = n->getLeft();
        }
        else {
            child = n->getRight(); // Can be nullptr.
        }
        // If the current node is the root:
        if (n->getParent() == nullptr) {
            root = child;
            child->setParent(nullptr);
        }
        else {
            Node<T> *parent = n->getParent();
            if (n == parent->getLeft()) {
                parent->setLeft(child);
                //if (child)
                //    child->setParent(parent);
            }
            else {
                parent->setRight(child);
                //if (child)
                //    child->setParent(parent);
            }
        }

        // Now we can safely delete *n.
        delete n;
        size = size - 1;
    }

    // remove_rec does not yet perform rotations, so it may break the AVL
    // balance property.
    // TODO: balance(x) from deletion place up to root.
    Node<T> *remove_rec(Node<T> *curr, Node<T> *node)
    {
        if (curr == nullptr) {
            // Node was not found...
            return nullptr;
        }
        // If the node to remove is smaller than current node, recurse left.
        if (*node < *curr) {
            curr->setLeft( remove_rec(curr->getLeft(), node) );
        }
        // If the node to remove is larger than current node, recurse right.
        else
        if (*node > *curr) {
            curr->setRight( remove_rec(curr->getRight(), node) );
        }
        // Otherwise, we have found our node. Remove it...
        else {
            // We are going to delete a node. There are basically 3 possible
            // cases:
            // 1) curr has no children. Simply delete it.
            // 2) curr has 1 child, delete curr and link parent to this child.
            // 3) curr has 2 children, it's a bit more tricky. First, swap
            // curr with getMin(curr->getRight). Now curr will not have a
            // left child, so removing curr is as easy as the second case.
            if (curr->getChildrenNumber() == 0) {
                deleteNodeWithOneChildOrNone(curr);
                return nullptr;
            } else
            if (curr->getChildrenNumber() == 1) {
                Node<T> *child = (curr->getLeft() != nullptr) ?
                    curr->getLeft() : curr->getRight();
                deleteNodeWithOneChildOrNone(curr);

                // Need to updateLocalData of path to deleted node.

                // Instead of returning, we set curr = child, and allow the
                // function to complete, specifically calling the
                // updateLocalData() function.
                //return child;
                curr = child;
            } else
            if (curr->getChildrenNumber() == 2) {
                Node<T> *minInRightSubtree = getMin(curr->getRight());
                swapNodes(curr, minInRightSubtree);
                remove_rec(minInRightSubtree->getRight(), curr);
                curr = minInRightSubtree;
            }
        }
        curr->updateLocalData();
        return curr;
    }
    void remove(const T &data)
    {
        Node<T> nodeToDelete(data);
        this->root = remove_rec(this->root, &nodeToDelete);
    }

    Node<T> *find_rec(Node<T> *curr, const T &data)
    {
        if (curr == nullptr) {
            return nullptr;
        }
        if (data < curr->getData()) {
            return find_rec(curr->getLeft(), data);
        }
        else
        if (data > curr->getData()) {
            return find_rec(curr->getRight(), data);
        }
        else {
            return curr;
        }
    }
    Node<T> *find(const T &data)
    {
        return find_rec(this->root, data);
    }
private:
}; // Tree.

} // namespace AVL.

#endif

